#!/usr/bin/env python3

'''
An interactive urwid based directory synchronizer
'''

APP_NAME = 'udsync'
__version__ = '0.3.3'
